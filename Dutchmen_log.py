import time
import datetime
import logging

logging.basicConfig(filename='Simulator_main.log', filemode='a', level=logging.INFO)
def log(msg):
    logging.info(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")+"  "+ msg)

def log_info(msg):
        logging.info(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S") + "  " + "INFO    : "+ msg)

def log_error(msg):
        logging.info(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S") + "  " + "ERROR   : "+ msg)

def log_cmd(msg):
        logging.info(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S") + "  " + "CMD     : "+ msg)

def log_warning(msg):
        logging.info(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S") + "  " + "WARNING : "+ msg)


def log_rerun():
        logging.info(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S") + "  " + "CMD     : " + "RERUN_the_test_case")

def info(msg):
        logging.info(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S") + "  " + "INFO    : "+ msg)

def error(msg):
        logging.info(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S") + "  " + "ERROR   : "+ msg)
